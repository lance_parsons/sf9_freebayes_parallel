#!/bin/bash

tmp_outdir="results/vcf/tmp"
mkdir -p "${tmp_outdir}"

/bin/ls -1 data/bams/*merged.bam > bam_list.txt

while read region; do
    echo $region
    freebayes_command="freebayes \
        -L \"bam_list.txt\" \
        -f \"data/dmel_genome/dmel-all-chromosome-r6.14.fa\" \
        --region \"${region}\" \
        --use-best-n-alleles 4 \
        --haplotype-length 0 \
        --min-alternate-count 1 \
        --min-alternate-fraction 0 \
        --pooled-continuous"
    if [ ! -z "${VARIANT_INPUT}" ]; then 
        freebayes_command="${freebayes_command} \
            --variant-input \"${VARIANT_INPUT}\" \
            --only-use-input-alleles"
    fi
    freebayes_command="${freebayes_command} \
        | gzip > \"${tmp_outdir}/${region}.vcf.gz\""

    sbatch --wrap="${freebayes_command}"
    #    --report-monomorphic \
done <region_list.txt

