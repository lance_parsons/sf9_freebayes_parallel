#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import allel
import numpy as np
import pandas as pd
import gzip


def main():
    """
    Parse command line args and calculate allele frequency
    Output:
        af_outfile - Table of allele frequency (RO/RO+AO) for each group
        count_outfile - Table of count of samples in each group with DP > 0
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("vcffile")
    parser.add_argument("af_outfile")
    parser.add_argument("count_outfile")
    parser.add_argument("--header", action='store_true', default=False)
    parser.add_argument("--header-only", action='store_true', default=False)
    parser.add_argument("--sample-group-start", type=int, default=22)
    parser.add_argument("--sample-group-end", type=int, default=30)
    parser.add_argument("--region", default=None)
    args = parser.parse_args()

    callset = allel.read_vcf(
            input=args.vcffile,
            alt_number=1,
            region=args.region,
            fields=['calldata/RO', 'calldata/AO', 'samples',
                    'variants/CHROM', 'variants/POS', 'variants/REF',
                    'variants/ALT', 'variants/QUAL', 'variants/DP'])
    if callset is not None and 'calldata/RO' in callset:
        # print("Calldata samples: {}".format(callset['samples']))
        sample_pool_ids = slicer_vectorized(
            callset['samples'].astype(str),
            args.sample_group_start,
            args.sample_group_end)
        print("\nsample_pool_ids: {}".format(sample_pool_ids))
        try:
            assert len(sample_pool_ids) > 0
        except AssertionError:
            raise RuntimeError("No pool ids were found, check "
                               "--sample-group-start and "
                               "--sample-group-end parameters")
        pools, pools_idx, pools_inv = np.unique(
                sample_pool_ids, return_index=True, return_inverse=True)

        # Calculate allele frequncies for each group
        af = np.divide(callset['calldata/RO'],
                       np.add(callset['calldata/RO'],
                              callset['calldata/AO']))
        af[callset['calldata/RO'] == -1] = np.nan

        # print("Shape of pools_inv: {}".format(pools_inv.shape))
        # print("Shape of af: {}".format(af.shape))
        # print("af.T: {}".format(af.T))
        pool_df = pd.DataFrame(np.concatenate(
                               (pools_inv[:, None], af.T), axis=1))
        # print("Pool DF: {}".format(pool_df))
        mean_af = pool_df.groupby(0).mean().T.reset_index(drop=True)
        # for i, p in enumerate(sample_pool_ids):
        #     print("{}:{}:{}".format(i, p, pools_inv[i]),)
        # print("\nsample_pool_ids: {}".format(sample_pool_ids))
        # print("pools_idx: {}".format(pools_idx))
        # print("pools_inv: {}".format(pools_inv))
        # print("pools: {}".format(pools))
        mean_af.columns = sample_pool_ids[pools_idx]
        # print("mean_af.columns: {}".format(mean_af.columns))
        df = pd.concat([pd.DataFrame({'CHROM': callset['variants/CHROM'],
                                      'POS': callset['variants/POS'],
                                      'REF': callset['variants/REF'],
                                      'ALT': callset['variants/ALT'],
                                      'QUAL': callset['variants/QUAL'],
                                      'DP': callset['variants/DP']}),
                        mean_af], axis=1, copy=False)

        # Calculate samples per group with DP > 1
        counts = np.add(callset['calldata/RO'], callset['calldata/AO'])
        counts[counts > 0] = 1
        counts[counts <= 0] = 0
        counts_pool_df = pd.DataFrame(np.concatenate(
                                      (pools_inv[:, None], counts.T), axis=1))
        sum_counts = counts_pool_df.groupby(0).sum().T.reset_index(drop=True)
        sum_counts.columns = sample_pool_ids[pools_idx]
        count_df = pd.concat([pd.DataFrame({'CHROM': callset['variants/CHROM'],
                                            'POS': callset['variants/POS'],
                                            'REF': callset['variants/REF'],
                                            'ALT': callset['variants/ALT'],
                                            'QUAL': callset['variants/QUAL'],
                                            'DP': callset['variants/DP']}),
                             sum_counts], axis=1, copy=False)

        if args.header_only:
            header = '{}\n'.format('\t'.join(df.columns))
            with gzip.open(args.af_outfile, 'wt') as outfile:
                outfile.write(header)
            with gzip.open(args.count_outfile, 'wt') as outfile:
                outfile.write(header)
        else:
            df.to_csv(args.af_outfile, sep='\t', index=False,
                      compression='gzip',
                      header=args.header)
            count_df.to_csv(args.count_outfile, sep='\t', index=False,
                            compression='gzip',
                            header=args.header)
    else:
        gzip.open(args.af_outfile, 'w').close()
        gzip.open(args.count_outfile, 'w').close()


def slicer_vectorized(a, start, end):
    b = a.view((str, 1)).reshape(len(a), -1)[:, start:end]
    return np.frombuffer(b.tostring(), dtype=(str, end-start))

if __name__ == "__main__":
    main()
