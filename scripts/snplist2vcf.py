#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import sys


def main():
    """TODO: Docstring for main.
    :returns: TODO

    """
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('snplist', nargs='?', metavar='SNPLIST_FILE',
                        type=argparse.FileType('r'), default=sys.stdin)
    parser.add_argument('vcf', nargs='?', metavar='VCF_FILE',
                        type=argparse.FileType('w'), default=sys.stdout)
    args = parser.parse_args()
    snplist2vcf(args.snplist, args.vcf)


def snplist2vcf(snplist, vcf):
    """TODO: Docstring for snplist2vcf.

    :snplist: TODO
    :vcf: TODO
    :returns: TODO

    """
    vcf.write('##fileformat=VCFv4.3\n')
    vcf.write('#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\n')
    for line in snplist:
        if line.startswith('CHROM'):
            continue
        fields = [x.strip() for x in line.split('\t')]
        vcf.write('{}\t{}\t.\t{}\t{}\t.\t.\t.\n'.format(
            fields[1], fields[2], fields[3], fields[4]))


if __name__ == "__main__":
    main()
