#!/bin/bash

# TODO Check for existence of config.yaml and cluster.yaml

# TODO Optional parameters to specify config and cluster files

export DRMAA_LIBRARY_PATH=/usr/local/slurm/16.05.8/lib/libdrmaa.so

snakemake --cluster-config 'cetus_cluster.yml' \
          --drmaa " --cpus-per-task={cluster.n} --mem={cluster.memory} --time={cluster.time} --qos={cluster.qos}" \
          --use-conda -w 90 -r -j 100 "$@"
          # --drmaa-log-dir 'slurm_logs' \

