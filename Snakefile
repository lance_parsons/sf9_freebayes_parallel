# The main entry point of your workflow for Parallel Freebayes analysis
# After configuring, running snakemake -n in a clone of this repository should
# successfully execute a dry-run of the workflow.
import tempfile

configfile: "config.yml"

regions = open(config["region_list"]).read().splitlines()
merged_bam_files = snakemake.utils.listfiles(config["merged_bam_file_pattern"])
pools = dict((y[0], x) for x, y in merged_bam_files)
sample_bam_files = snakemake.utils.listfiles(config["sample_bam_file_pattern"])
samples = dict((y[0], x) for x, y in sample_bam_files)

wildcard_constraints:
    group="[^_]+"

def get_merged_bam(wildcards):
    return pools[wildcards.pool]


rule all:
    input:
        config["output_dir"] + "/merged.vcf.gz.tbi",
        config["output_dir"] + "/population_allele_frequency.txt.gz",
        config["output_dir"] + "/population_count.txt.gz",
        config["output_dir"] + "/plate_allele_frequency.txt.gz",
        config["output_dir"] + "/plate_count.txt.gz"


rule freebayes:
    input:
        bam_file_list=config["output_dir"] + "/bam_file_list.txt",
        samples=config["output_dir"] + "/sorted_sample_list.txt",
        reference_fasta=config["reference_fasta_file"]
    output:
        temp(config["output_dir"] + "/tmp/{region}.vcf.gz")
    shell:
        "freebayes "
            "-L {input.bam_file_list:q} "
            "-f {input.reference_fasta:q} "
            "--region {wildcards.region:q} "
            "--use-best-n-alleles 2 "
            "--no-indels "
            "--no-mnps "
            "--no-complex "
            "--haplotype-length 0 "
            "--min-alternate-count 1 "
            "--min-alternate-fraction 0 | "
        "bcftools view "
            "--samples-file {input.samples:q} "
            "--output-file {output:q} "
            "--output-type z"

rule bam_file_list:
    input:
        pools.values()
    output:
        config["output_dir"] + "/bam_file_list.txt"
    run:
        with open(output[0], "wb") as tmp:
            for f in input:
                tmp.write('{}\n'.format(f).encode())

ruleorder: allele_frequency_from_merged_vcf > allele_frequency

rule allele_frequency:
    input:
        config["output_dir"] + "/tmp/{region}.vcf.gz"
    output:
        af=temp(config["output_dir"] + "/{group}_allele_frequency_tmp/{region}_{group}_allele_frequency.txt.gz"),
        count=temp(config["output_dir"] + "/{group}_allele_frequency_tmp/{region}_{group}_count.txt.gz")
    params:
        group_start=lambda wildcards: config['group_start']["{}".format(wildcards.group)],
        group_end=lambda wildcards: config['group_end']["{}".format(wildcards.group)],
    shell:
        "python scripts/allele_frequency.py "
        "--sample-group-start {params.group_start} "
        "--sample-group-end {params.group_end} "
        "{input:q} {output.af:q} {output.count:q}"

rule allele_frequency_from_merged_vcf:
    input:
        config["output_dir"] + "/merged.vcf.gz"
    output:
        af=temp(config["output_dir"] + "/{group}_allele_frequency_tmp/{region}_{group}_allele_frequency.txt.gz"),
        count=temp(config["output_dir"] + "/{group}_allele_frequency_tmp/{region}_{group}_count.txt.gz")
    params:
        group_start=lambda wildcards: config['group_start']["{}".format(wildcards.group)],
        group_end=lambda wildcards: config['group_end']["{}".format(wildcards.group)],
    shell:
        "python scripts/allele_frequency.py "
        "--region {wildcards.region:q} "
        "--sample-group-start {params.group_start} "
        "--sample-group-end {params.group_end} "
        "{input:q} {output.af:q} {output.count:q}"

ruleorder: allele_frequency_headers_from_merged_vcf > allele_frequency_headers

rule allele_frequency_headers:
    input:
        vcf=config["output_dir"] + "/tmp/{region}.vcf.gz".format(region=regions[0]),
    output:
        allele_frequency=temp(config["output_dir"] + "/{group}_allele_frequency_headers.txt.gz"),
        count=temp(config["output_dir"] + "/{group}_count_headers.txt.gz")
    params:
        group_start=lambda wildcards: config['group_start']["{}".format(wildcards.group)],
        group_end=lambda wildcards: config['group_end']["{}".format(wildcards.group)],
    shell:
        "python scripts/allele_frequency.py "
            "--sample-group-start {params.group_start} "
            "--sample-group-end {params.group_end} "
            "{input.vcf:q} {output.allele_frequency:q} {output.count:q} --header-only"

rule allele_frequency_headers_from_merged_vcf:
    input:
        vcf=config["output_dir"] + "/merged.vcf.gz"
    output:
        allele_frequency=temp(config["output_dir"] + "/{group}_allele_frequency_headers.txt.gz"),
        count=temp(config["output_dir"] + "/{group}_count_headers.txt.gz")
    params:
        group_start=lambda wildcards: config['group_start']["{}".format(wildcards.group)],
        group_end=lambda wildcards: config['group_end']["{}".format(wildcards.group)],
        region=regions[0],
    shell:
        "python scripts/allele_frequency.py "
            "--region {params.region:q} "
            "--sample-group-start {params.group_start} "
            "--sample-group-end {params.group_end} "
            "{input.vcf:q} {output.allele_frequency:q} {output.count:q} --header-only"

rule combine_allele_frequencies:
    input:
        headers=config["output_dir"] + "/{group}_allele_frequency_headers.txt.gz",
        allele_frequency=expand(config["output_dir"] + "/{{group}}_allele_frequency_tmp/{region}_{{group}}_allele_frequency.txt.gz", region=regions)
    output:
        protected(config["output_dir"] + "/{group}_allele_frequency.txt.gz")
    run:
        shell("cp {input.headers:q} {output:q}")
        with tempfile.NamedTemporaryFile() as tmp:
            for f in input['allele_frequency']:
                tmp.write('{}\n'.format(f).encode())
            tmp.flush()
            shell("xargs < {tmp.name:q} cat >> {output:q}")

rule combine_counts:
    input:
        headers=config["output_dir"] + "/{group}_count_headers.txt.gz",
        count=expand(config["output_dir"] + "/{{group}}_allele_frequency_tmp/{region}_{{group}}_count.txt.gz", region=regions)
    output:
        protected(config["output_dir"] + "/{group}_count.txt.gz")
    run:
        shell("cp {input.headers:q} {output:q}")
        with tempfile.NamedTemporaryFile() as tmp:
            for f in input['count']:
                tmp.write('{}\n'.format(f).encode())
            tmp.flush()
            shell("xargs < {tmp.name:q} cat >> {output:q}")


rule sorted_sample_list:
    input:
        samples.values()
    output:
        config["output_dir"] + "/sorted_sample_list.txt"
    run:
        with open(output[0], "wb") as tmp:
            for f in sorted(samples):
                tmp.write('{}\n'.format(f).encode())


rule combine_vcf:
    input:
        expand(config["output_dir"] + "/tmp/{region}.vcf.gz", region=regions)
    output:
        vcf=protected(config["output_dir"] + "/merged.vcf.gz")
    threads:
        4
    run:
        compression_threads = threads - 1
        with tempfile.NamedTemporaryFile() as tmp:
            for f in input:
                tmp.write('{}\n'.format(f).encode())
            tmp.flush()
            shell("cat {tmp.name:q}")
            shell("bcftools concat -f {tmp.name:q} -Oz --threads {compression_threads} -o {output.vcf:q}")

rule list_vcf:
    input:
        expand(config["output_dir"] + "/tmp/{region}.vcf.gz", region=regions)
    output:
        "vcf_files.txt"
    run:
        with open(output[0], "wb") as tmp:
            for f in input:
                tmp.write('{}\n'.format(f).encode())

rule combine_vcf_to_bcf:
    input:
        expand(config["output_dir"] + "/tmp/{region}.vcf.gz", region=regions)
    output:
        protected(config["output_dir"] + "/merged.bcf")
    threads:
        6
    run:
        compression_threads = threads - 1
        with tempfile.NamedTemporaryFile() as tmp:
            for f in input:
                tmp.write('{}\n'.format(f).encode())
            tmp.flush()
            shell("bcftools concat -f {tmp.name:q} -Ob --threads {compression_threads} -o {output:q}")

rule tabix:
    input:
        "{file}"
    output:
        "{file}.tbi"
    shell:
        "tabix -f {input:q}"
