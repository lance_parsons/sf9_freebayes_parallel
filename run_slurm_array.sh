#!/bin/bash

set -o nounset
set -o errexit

# CONFIGURATION
export OUTPUT_DIR="results/minjasSelection/2019-03-15_currentGeneration_subset_prio1"
mkdir -p "${OUTPUT_DIR}"

export REGION_LIST_FILE="region_list_100000.txt"
#$(fasta_generate_regions.py data/dmel_genome/dmel-all-chromosome-r6.14.fa.fai 10000 > "${REGION_LIST_FILE}")
#export REGION_LIST_FILE="large_regions.txt"
#export REGION_LIST_FILE="rerun_regions.txt"

export BAM_LIST_FILE="${OUTPUT_DIR}/bam_list.txt"
/bin/ls -1 data/minjasSelection/2019-03-15_currentGeneration/mapped/merged/*.bam > ${BAM_LIST_FILE}

export VARIANT_INPUT="data/minjasSelection/snpSubset_prio1.vcf.gz"

# MAIN

function join_by { local IFS="$1"; shift; echo "$*"; }

export SLURM_LOG_DIR="${OUTPUT_DIR}/slurm_logs"
mkdir -p "${SLURM_LOG_DIR}"
export FREEBAYES_TMP_OUTDIR="${OUTPUT_DIR}/tmp"
mkdir -p "${FREEBAYES_TMP_OUTDIR}"
VCF_OUTFILE="${OUTPUT_DIR}/vcf/merged.vcf.gz"

NUM_REGIONS=$(wc -l < "${REGION_LIST_FILE}")
# NUM_REGIONS=5
echo "Total regions: $NUM_REGIONS"
SLURM_ARRAY_MAX="2000"
echo "SLURM array max: $SLURM_ARRAY_MAX"

JID_LIST=""
i="0"
stop="0"
while [ $stop -lt $NUM_REGIONS ]; do
    start=$[($i * $SLURM_ARRAY_MAX) + 1]
    stop=$[($i * $SLURM_ARRAY_MAX) + $SLURM_ARRAY_MAX]
    stop=$(( $stop < $NUM_REGIONS ? $stop : $NUM_REGIONS ))
    count=$[($stop - $start) + 1]
    echo "array 1-$count = $start-$stop"
    # Retry if sbatch fails (which it is prone to do)
    JID=$(sbatch --array=1-${count}%100 --parsable -o "${SLURM_LOG_DIR}/freebayes_%A_%a.out" -e "${SLURM_LOG_DIR}/freebayes_%A_%a.err" freebayes_arrayjob.sh $start);
    if [ $? -eq 0 ]; then
        i=$((i+1))
        retry=1
    else
        if [ $retry <= 5 ]; then
            echo "Last submit failed, retrying"
            retry=$((retry+1))
        else
            echo "Retried maximum of $retry times, moving on."
            i=$((i+1))
        fi
    fi
    JID_LIST="$JID_LIST $JID"
    sleep 30 # SLURM doesn't like too many array jobs being submitted at one time
done

# Use Snakemake to combine (this doesn't sort reliably)
#JID_STRING=$(join_by : $JID_LIST)
#echo "JID string: $JID_STRING"
#sbatch --dependency=afterok:$JID_STRING --wrap="find ${FREEBAYES_TMP_OUTDIR}/ -name '*.vcf.gz' -print0 | xargs -0 zcat | vcffirstheader | vcfstreamsort -w 20000 | vcfuniq | gzip > ${VCF_OUTFILE}"

