#!/bin/bash
#SBATCH -J freebayes
#SBATCH -n 1
#SBATCH -N 1-1
#SBATCH --mem 50000
#SBATCH --time 0-24:00
#SBATCH --mail-user lparsons@princeton.edu
#SBATCH --mail-type ALL

set -o nounset
set -o errexit

# FREEBAYES_TMP_OUTDIR="${FREEBAYES_TMP_OUTDIR:-"results/vcf/tmp"}"
# mkdir -p "${FREEBAYES_TMP_OUTDIR}"
# REGION_LIST_FILE="${REGION_LIST_FILE:-"region_list.txt"}"
# BAM_LIST_FILE="${BAM_LIST_FILE:-"bam_list.txt"}"

start=$1
region_number=$[ ($start + ${SLURM_ARRAY_TASK_ID}) - 1 ]
region=$(sed -n ${region_number}p "${REGION_LIST_FILE}")

echo $region
freebayes_command="freebayes \
    -L \"${BAM_LIST_FILE}\" \
    -f \"data/dmel_genome/dmel-all-chromosome-r6.14.fa\" \
    --region \"${region}\" \
    --use-best-n-alleles 2 \
    --no-indels \
    --no-mnps \
    --no-complex \
    --haplotype-length 0 \
    --min-alternate-count 1 \
    --min-alternate-fraction 0"
if [ ! -z "${VARIANT_INPUT}" ]; then 
    freebayes_command="${freebayes_command} \
        --variant-input \"${VARIANT_INPUT}\" \
        --only-use-input-alleles"
fi
freebayes_command="${freebayes_command} \
    | gzip > \"${FREEBAYES_TMP_OUTDIR}/${region}.vcf.gz\""
echo $freebayes_command
eval $freebayes_command
  # --report-monomorphic \
  # --pooled-continuous \
